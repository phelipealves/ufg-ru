package br.com.divpix;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class DaoRuGenerator {
    public static void main(String[] args){

        Schema schema = new Schema(1, "br.com.divpix.ufgru.models");

        addWeekDayMeal(schema);

        try {
            new DaoGenerator().generateAll(schema, "app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addWeekDayMeal(Schema schema){
        schema.enableKeepSectionsByDefault();
        Entity week = schema.addEntity("Week");
        week.addIdProperty().notNull();
        week.addIntProperty("year").notNull();
        week.addIntProperty("number").notNull();

        Entity day = schema.addEntity("Day");
        day.addIdProperty().notNull();
        day.addBooleanProperty("open").notNull();
        day.addStringProperty("message");
        day.addStringProperty("date").notNull();
        Property weekDay = day.addIntProperty("weekDay").notNull().getProperty();
        Property weekId = day.addLongProperty("weekId").notNull().getProperty();
        day.addToOne(week, weekId);

        Entity meal = schema.addEntity("Meal");
        meal.addIdProperty().notNull();
        meal.addStringProperty("title").notNull();
        meal.addStringProperty("type").notNull();
        meal.addBooleanProperty("open").notNull();
        meal.addStringProperty("message");
        meal.addStringProperty("starts").notNull();
        meal.addStringProperty("ends").notNull();
        meal.addStringProperty("menu");
        Property dayId = meal.addLongProperty("dayId").notNull().getProperty();
        meal.addToOne(day, dayId);

        ToMany weekToDays = week.addToMany(day, weekId);
        weekToDays.setName("days");
        weekToDays.orderAsc(weekDay);

        ToMany dayToMeals = day.addToMany(meal, dayId);
        dayToMeals.setName("meals");
    }
}
