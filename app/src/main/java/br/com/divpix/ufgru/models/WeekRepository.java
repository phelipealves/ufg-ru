package br.com.divpix.ufgru.models;

import android.content.Context;

import java.util.List;

import br.com.divpix.ufgru.UfgRuApplication;

/**
 * Created by phelipe on 03/05/2015.
 */
public class WeekRepository {

    public static void insertOrUpdate(Context context, Week week) {
        getWeekDao(context).insertOrReplace(week);
    }

    public static void clearDays(Context context) {
        getWeekDao(context).deleteAll();
    }

    public static void deleteWeekWithId(Context context, long id) {
        getWeekDao(context).delete(getWeekForId(context, id));
    }

    public static List<Week> getAllWeeks(Context context) {
        return getWeekDao(context).loadAll();
    }

    public static Week getWeekForId(Context context, long id) {
        return getWeekDao(context).load(id);
    }

    private static WeekDao getWeekDao(Context c) {
        return ((UfgRuApplication) c.getApplicationContext()).getDaoSession().getWeekDao();
    }
}
