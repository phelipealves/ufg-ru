package br.com.divpix.ufgru.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.models.Day;

/**
 * Created by phelipe on 10/05/2015.
 */
public class DayAdapter extends RecyclerView.Adapter<DayViewHolder> {

    private Context mContext;
    private List<Day> mDays;

    public DayAdapter() {
    }

    public DayAdapter(List<Day> occurrences) {
        this.mDays = occurrences;
    }

    public void updateData(List<Day> occurrences) {
        this.mDays = occurrences;
        notifyDataSetChanged();
    }

    @Override
    public DayViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View rowView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_day, viewGroup, false);

        //set the mContext
        this.mContext = viewGroup.getContext();

        return new DayViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(DayViewHolder holder, int position) {
        Day day = mDays.get(position);
        holder.dayDateTextView.setText(day.getDate());
    }

    @Override
    public int getItemCount() {
        return mDays.size();
    }
}

class DayViewHolder extends RecyclerView.ViewHolder {

    TextView dayDateTextView;

    public DayViewHolder(View itemView) {
        super(itemView);
        dayDateTextView = (TextView) itemView.findViewById(R.id.dayDateTextView);
    }
}
