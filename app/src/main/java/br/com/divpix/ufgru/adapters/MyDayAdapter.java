package br.com.divpix.ufgru.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.models.Day;

/**
 * Created by phelipe on 5/11/15.
 */
public class MyDayAdapter extends ArrayAdapter<Day> {

    private static int LAYOUT = R.layout.item_day;

    private Activity mContext;
    private List<Day> mDays;

    public MyDayAdapter(Activity context, List<Day> days) {
        super(context, LAYOUT, days);
        mContext = context;
        mDays = days;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            final LayoutInflater inflater = mContext.getLayoutInflater();
            view = inflater.inflate(LAYOUT, null);

            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Day day = mDays.get(position);
        viewHolder.setDayDateTextView(day.getDate());

        return view;
    }

    private class ViewHolder {
        private TextView dayDateTextView;

        public ViewHolder(View view) {
            dayDateTextView = (TextView) view.findViewById(R.id.dayDateTextView);
        }

        public void setDayDateTextView(String number) {
            this.dayDateTextView.setText(number);
        }
    }

}
