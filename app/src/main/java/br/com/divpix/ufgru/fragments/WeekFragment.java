package br.com.divpix.ufgru.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.adapters.DayAdapter;
import br.com.divpix.ufgru.adapters.MyDayAdapter;
import br.com.divpix.ufgru.models.Day;
import br.com.divpix.ufgru.models.Week;
import br.com.divpix.ufgru.models.WeekRepository;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link br.com.divpix.ufgru.fragments.WeekFragment.OnDayOfWeekSelectedListener} interface
 * to handle interaction events.
 * Use the {@link WeekFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeekFragment extends Fragment {

    private static String LOG_TAG = WeekFragment.class.getSimpleName();

    private static final String ARG_WEEK_ID = "week_id";

    private long mWeekId;

    private OnDayOfWeekSelectedListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param weekId Identifier of the Week
     * @return A new instance of fragment WeekFragment.
     */
    public static WeekFragment newInstance(long weekId) {
        WeekFragment fragment = new WeekFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_WEEK_ID, weekId);
        fragment.setArguments(args);
        return fragment;
    }

    public WeekFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mWeekId = getArguments().getLong(ARG_WEEK_ID);
        }
    }

//    private RecyclerView mRecyclerView;
//    private DayAdapter mDayAdapter;
//    private RecyclerView.LayoutManager mLayoutManager;

    private MyDayAdapter dayAdapter;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_week, container, false);
        ButterKnife.inject(this, view);

//        mRecyclerView = (RecyclerView) view.findViewById(R.id.week_days_recycler_view);
//        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        // specify an adapter
//        Week week = WeekRepository.getWeekForId(getActivity(), mWeekId);
//        mDayAdapter = new DayAdapter(week.getDays());
//
//        mRecyclerView.setAdapter(mDayAdapter);

        Week week = WeekRepository.getWeekForId(getActivity(), mWeekId);
        dayAdapter = new MyDayAdapter(getActivity(), week.getDays());

        listView = (ListView) view.findViewById(R.id.week_days_list_view);
        listView.setAdapter(dayAdapter);
        listView.setOnItemClickListener(new ItemClickListener());

        return view;
    }

    /**
     * Classe que implementa o listener para o item que for selecionado
     */
    private class ItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            Day day = (Day) parent.getAdapter().getItem(position);
            if (day != null) {
                onDayPressed(day.getId());
            }
        }
    }

    public void onDayPressed(long dayId) {
        if (mListener != null) {
            mListener.OnDayOfWeekSelected(dayId);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnDayOfWeekSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDayOfWeekSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    /**
     *
     */
    public interface OnDayOfWeekSelectedListener {
        public void OnDayOfWeekSelected(long dayId);
    }

}
