package br.com.divpix.ufgru.api;

import br.com.divpix.ufgru.models.Week;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by phelipe on 10/05/2015.
 */
public interface RuApi {

    public static String SERVER_URL = "http://cruf.tk/api/v1";

    @GET("/week")
    void getWeek(Callback<Week> week);
}
