package br.com.divpix.ufgru.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.fragments.MealFragment;
import br.com.divpix.ufgru.fragments.WeekFragment;
import br.com.divpix.ufgru.models.Day;
import br.com.divpix.ufgru.models.DayRepository;
import br.com.divpix.ufgru.ui.widget.SlidingTabLayout;


public class DayActivity extends ActionBarActivity {

    private static String LOG_TAG = DayActivity.class.getSimpleName();

    public static final String ARG_DAY_ID = "day_id";

    Toolbar toolbar;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    ViewPager pager;
    CharSequence[] titles = {"Lunch", "Dinner"};
    int numOfTabs = 2;

    long mDayId;
    Day mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day);
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdapter(getSupportFragmentManager(), titles, numOfTabs, this);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorAccent);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);


        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mDayId = bundle.getLong(ARG_DAY_ID);
            mDay = DayRepository.getDayForId(this, mDayId);
            Log.i(LOG_TAG, "Day id: " + mDay.toString());
            getSupportActionBar().setTitle("Segunda");
            getSupportActionBar().setSubtitle(mDay.getDate());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private static final int LUNCH_TAB = 0;
        private static final int DINNER_TAB = 1;

        private Context mContext;
        private CharSequence mTitles[];
        private int mNumOfTabs;

        public ViewPagerAdapter(FragmentManager fm, CharSequence[] titles, int numOfTabs, Context context) {
            super(fm);

            this.mTitles = titles;
            this.mNumOfTabs = numOfTabs;
            this.mContext = context;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case LUNCH_TAB:
                    return MealFragment.newInstance(4);
                case DINNER_TAB:
                    return MealFragment.newInstance(4);
                default:
                    return new DemoFragment();
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case LUNCH_TAB:
                    return mContext.getString(R.string.lunch);
                case DINNER_TAB:
                    return mContext.getString(R.string.dinner);
                default:
                    return mContext.getString(R.string.meal);
            }
        }
    }
}


