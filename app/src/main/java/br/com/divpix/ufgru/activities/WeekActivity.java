package br.com.divpix.ufgru.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.fragments.WeekFragment;

public class WeekActivity extends ActionBarActivity implements WeekFragment.OnDayOfWeekSelectedListener {

    private static String LOG_TAG = WeekActivity.class.getSimpleName();

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week);

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, WeekFragment.newInstance(8))
                    .commit();
        }
    }

    @Override
    public void OnDayOfWeekSelected(long dayId) {
        Intent intent = new Intent(this, DayActivity.class);
        Bundle args = new Bundle();
        args.putLong(DayActivity.ARG_DAY_ID, dayId);
        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_week, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
