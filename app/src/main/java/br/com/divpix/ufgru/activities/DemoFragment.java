package br.com.divpix.ufgru.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.divpix.ufgru.R;
import br.com.divpix.ufgru.api.ApiFactory;
import br.com.divpix.ufgru.models.Day;
import br.com.divpix.ufgru.models.DayRepository;
import br.com.divpix.ufgru.models.Meal;
import br.com.divpix.ufgru.models.MealRepository;
import br.com.divpix.ufgru.models.Week;
import br.com.divpix.ufgru.models.WeekRepository;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by phelipe on 09/05/2015.
 */
public class DemoFragment extends Fragment {

    private static String LOG_TAG = DemoFragment.class.getSimpleName();
    private Context mContext;
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(
                R.layout.fragment_collection_object, container, false);
        textView = (TextView) rootView.findViewById(R.id.text1);
        textView.setText("Página");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();
        loadWeek();
    }

    private void loadWeek(){
        ApiFactory.getFactory().getWeek(new Callback<Week>() {
            @Override
            public void success(Week week, Response response) {
                DayRepository.getDayForId(mContext, 1);
                insertNewWeek(week);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(LOG_TAG, error.toString());
                textView.setText("Error " + error.toString());
            }
        });
    }

    private void insertNewWeek(Week week){
        Week testWeek = WeekRepository.getWeekForId(mContext, week.getId());
        if(testWeek == null){
            // Inserindo a nova semana
            WeekRepository.insertOrUpdate(mContext, week);
            for (Day day : week.getDays()){
                day.setWeek(week);
                DayRepository.insertOrUpdate(mContext, day);
                for (Meal meal : day.getMeals()){
                    meal.setDay(day);
                    MealRepository.insertOrUpdate(mContext, meal);
                }
            }
            printWeekInView(week.getId());
        }
        else{
            Log.e(LOG_TAG, "Week already in the database");
            printWeekInView(week.getId());
        }
    }

    private void printWeekInView(long id){
        Week week = WeekRepository.getWeekForId(mContext, id);
        textView.append(week.toString());
        textView.append("\n\n");
        for (Day day : week.getDays()){
            textView.append(day.toString());
            textView.append("\n");
            for (Meal meal : day.getMeals()){
                textView.append(meal.toString());
                textView.append("\n");
            }
        }
    }
}
