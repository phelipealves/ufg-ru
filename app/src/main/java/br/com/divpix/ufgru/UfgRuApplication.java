package br.com.divpix.ufgru;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import br.com.divpix.ufgru.models.DaoMaster;
import br.com.divpix.ufgru.models.DaoSession;

/**
 * Created by phelipe on 03/05/2015.
 */
public class UfgRuApplication extends Application {

    DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
    }

    private void setupDatabase(){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "ufgru-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
