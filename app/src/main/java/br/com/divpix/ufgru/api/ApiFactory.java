package br.com.divpix.ufgru.api;

import retrofit.RestAdapter;

/**
 * Created by phelipe on 10/05/2015.
 */
public class ApiFactory {

    public static RuApi getFactory() {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(RuApi.SERVER_URL).build();
        return (RuApi) restAdapter.create(RuApi.class);
    }

}
