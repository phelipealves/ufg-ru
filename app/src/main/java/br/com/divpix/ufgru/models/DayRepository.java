package br.com.divpix.ufgru.models;

import android.content.Context;

import java.util.List;

import br.com.divpix.ufgru.UfgRuApplication;

/**
 * Created by phelipe on 03/05/2015.
 */
public class DayRepository {

    public static void insertOrUpdate(Context context, Day day) {
        getDayDao(context).insertOrReplace(day);
    }

    public static void clearDays(Context context) {
        getDayDao(context).deleteAll();
    }

    public static void deleteDayWithId(Context context, long id) {
        getDayDao(context).delete(getDayForId(context, id));
    }

    public static List<Day> getAllDays(Context context) {
        return getDayDao(context).loadAll();
    }

    public static Day getDayForId(Context context, long id) {
        return getDayDao(context).load(id);
    }

    private static DayDao getDayDao(Context c) {
        return ((UfgRuApplication) c.getApplicationContext()).getDaoSession().getDayDao();
    }
}
