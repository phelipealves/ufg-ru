package br.com.divpix.ufgru.models;

import android.content.Context;

import java.util.List;

import br.com.divpix.ufgru.UfgRuApplication;

/**
 * Created by phelipe on 03/05/2015.
 */
public class MealRepository {

    public static void insertOrUpdate(Context context, Meal meal) {
        getMealDao(context).insertOrReplace(meal);
    }

    public static void clearMeals(Context context) {
        getMealDao(context).deleteAll();
    }

    public static void deleteMealWithId(Context context, long id) {
        getMealDao(context).delete(getBoxForId(context, id));
    }

    public static List<Meal> getAllMeals(Context context) {
        return getMealDao(context).loadAll();
    }

    public static Meal getBoxForId(Context context, long id) {
        return getMealDao(context).load(id);
    }

    private static MealDao getMealDao(Context c) {
        return ((UfgRuApplication) c.getApplicationContext()).getDaoSession().getMealDao();
    }
}
